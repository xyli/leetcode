class Solution(object):
    def isAlienSorted(self, words, order):
        """
        :type words: List[str]
        :type order: str
        :rtype: bool
        """
        d = {c: i for i, c in enumerate(order)}

        cur = ''.join([chr(ord('a') + d[c]) for c in words[0]])

        for w in words:
            temp = ''.join([chr(ord('a') + d[c]) for c in w])
            if temp < cur:
                return False
            cur = temp

        return True
