class Solution(object):
    def canReorderDoubled(self, A):
        """
        :type A: List[int]
        :rtype: bool
        """
        d = collections.Counter(A)
        nums = sorted(d.keys(), key=abs)

        for i in nums:
            if d[i] == 0:
                continue
            if 2 * i in d and d[2 * i] >= d[i]:
                d[2 * i] -= d[i]
            else:
                return False
