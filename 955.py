class Solution(object):
    def minDeletionSize(self, A):
        """
        :type A: List[str]
        :rtype: int
        """
        def is_sorted(arr, i):
            cur = [arr[0]]
            tier = []
            for s in arr[1:]:
                if s[i] < cur[-1][i]:
                    return False, []
                elif s[i] == cur[-1][i]:
                    cur.append(s)
                else:
                    tier.append(cur)
                    cur = [s]
            if len(cur) > 1:
                tier.append(cur)
            return True, tier

        tier = [A]
        n = len(A[0])
        res = 0

        for i in range(n):
            temp_tier = []
            d = True
            if not tier:
                return res
            for t in tier:
                d, l = is_sorted(t, i)
                if not d:
                    res += 1
                    break
                else:
                    temp_tier += l
            if d:
                tier = temp_tier

        return res
