class Solution(object):
    def tallestBillboard(self, rods):
        """
        :type rods: List[int]
        :rtype: int
        """
        max_abs = {0: 0}

        for r in rods:
            for k, v in max_abs.items():
                max_abs[k + r] = max(max_abs.get(k + r, 0), v)
                max_abs[abs(k - r)] = max(max_abs.get(abs(k - r), 0),
                                          v + min(r, k))
        return max_abs[0]
